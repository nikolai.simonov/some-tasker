package com.harman.myapplication2;

import com.harman.myapplication2.repository.RepositoryFactory;
import com.harman.myapplication2.repository.mock.MockTaskRepository;
import com.harman.myapplication2.repository.real.TaskRepository;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class RepositoryFactoryTest {

    @Test
    public void getInstance() {
        int hash1 = RepositoryFactory.getRepository(MockTaskRepository.class).hashCode();
        int hash2 = RepositoryFactory.getRepository(MockTaskRepository.class).hashCode();

        assertEquals(hash1, hash2);

        int hash3 = RepositoryFactory.getRepository(TaskRepository.class).hashCode();

        assertNotEquals(hash1, hash3);
    }

}