package com.harman.myapplication2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.harman.myapplication2.model.Task;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends BaseAdapter {

    private final TaskAdapterCallback callback;

    public interface TaskAdapterCallback{
        void onTaskChanged(Task task);
    }

    static class ViewHolder {

        private final TextView label;
        private final CheckBox checkBox;
        private final TaskAdapterCallback callback;
        private Task task;

        ViewHolder(View itemView, TaskAdapterCallback callback) {
            this.label = itemView.findViewById(R.id.label);
            this.checkBox = itemView.findViewById(R.id.checkbox);

            this.callback = callback;

            this.checkBox.setOnCheckedChangeListener(this::onTaskChecked);
        }

        private void onTaskChecked(CompoundButton compoundButton, boolean b) {
            task.setDone(b);
            this.callback.onTaskChanged(task);
        }

        void bind(Task data){
            task = data;
            label.setText(data.getLabel());
            checkBox.setChecked(data.isDone());
        }
    }

    private final List<Task> items = new ArrayList<>();

    public MyAdapter(TaskAdapterCallback callback) {
        this.callback = callback;
    }

    public void setItems(List<Task> taskList){
        this.items.clear();
        if(taskList != null) {
            this.items.addAll(taskList);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if(convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_view, parent, false);

            viewHolder = new ViewHolder(convertView, callback);
            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.bind(items.get(position));

        return convertView;
    }
}
