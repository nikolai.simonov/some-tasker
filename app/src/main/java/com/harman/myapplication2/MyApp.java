package com.harman.myapplication2;

import android.app.Application;

import com.harman.myapplication2.das.FileDataStorage;

public class MyApp extends Application {

    private FileDataStorage fileDataStorage;

    @Override
    public void onCreate() {
        super.onCreate();
        fileDataStorage = new FileDataStorage(getApplicationInfo().dataDir);
    }

    public FileDataStorage getFileDataStorage() {
        return fileDataStorage;
    }
}
