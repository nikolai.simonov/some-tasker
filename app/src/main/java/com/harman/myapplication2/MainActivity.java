package com.harman.myapplication2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.harman.myapplication2.fragments.AddDialogFragment;
import com.harman.myapplication2.fragments.MyListFragment;
import com.harman.myapplication2.model.Task;
import com.harman.myapplication2.repository.IRepository;
import com.harman.myapplication2.repository.RepositoryFactory;
import com.harman.myapplication2.repository.real.TaskRepository;

import java.util.List;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FloatingActionButton fab = this.findViewById(R.id.fab_add);
        fab.setOnClickListener(this::onFabClick);

        this.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, new MyListFragment())
                .commit();

    }

    private void onFabClick(View view) {
        AddDialogFragment dialog = AddDialogFragment.newInstance(this::onAddHandler);
        dialog.show(this.getSupportFragmentManager(), "ADD_TASK_DIALOG");
    }

    private void onAddHandler(String s) {
        IRepository<Task> repository = RepositoryFactory.getRepository(TaskRepository.class, this);
        int nextId = findLastIndex(repository.getAll()) + 1;
        Task task = new Task(nextId, s, false);
        repository.save(task);
    }

    private int findLastIndex(List<Task> tasks){
        int max = 0;
        for (Task t : tasks){
            if(t.getId() > max){
                max = t.getId();
            }
        }
        return max;
    }
}
