package com.harman.myapplication2.model;

public class Task {
    private final int id;

    private String label;
    private boolean isDone;

    public Task(int id, String label, boolean isDone) {
        this.id = id;
        this.label = label;
        this.isDone = isDone;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public int getId() {
        return id;
    }
}
