package com.harman.myapplication2.repository.real;

import android.support.annotation.Nullable;

import com.harman.myapplication2.das.FileDataStorage;
import com.harman.myapplication2.model.Task;
import com.harman.myapplication2.repository.IRepository;
import com.harman.myapplication2.utils.BaseObservable;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends BaseObservable implements IRepository<Task> {
    private List<Task> list;
    private FileDataStorage storage;

    public TaskRepository() {
        list = new ArrayList<>();
    }

    public void init(FileDataStorage fds) {
        this.storage = fds;
        list = storage.readTasksFromFile();
    }

    @Override
    @Nullable
    public Task get(int id) {
        for (Task t: list) {
            if(t.getId() == id){
                return t;
            }
        }
        return null;
    }

    @Override
    public void save(Task item) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId() == item.getId()) {
                list.get(i).setLabel(item.getLabel());
                list.get(i).setDone(item.isDone());
                writeToDisk();
                return;
            }
        }
        list.add(item);
        writeToDisk();
    }

    @Override
    public void delete(Task item) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId() == item.getId()) {
                list.remove(i);
                writeToDisk();
                return;
            }
        }

    }

    @Override
    public List<Task> getAll() {
        return list;
    }

    private void writeToDisk(){
        storage.writeTasksToFile(list);
        onChanged();
    }
}
