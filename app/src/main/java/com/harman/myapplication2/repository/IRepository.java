package com.harman.myapplication2.repository;

import android.support.annotation.Nullable;

import com.harman.myapplication2.utils.Observable;

import java.util.List;

public interface IRepository<T> extends Observable {
    @Nullable T get(int id);
    void save(T item);
    void delete(T item);
    List<T> getAll();
}
