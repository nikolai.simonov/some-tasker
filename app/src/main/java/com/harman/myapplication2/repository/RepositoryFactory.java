package com.harman.myapplication2.repository;

import android.content.Context;
import android.support.annotation.Nullable;

import com.harman.myapplication2.MyApp;
import com.harman.myapplication2.repository.real.TaskRepository;

public class RepositoryFactory {

    private static IRepository<?> instance;

    public static synchronized <V> IRepository<V> getRepository(Class<? extends IRepository<V>> type, @Nullable Context context) {
        if (instance == null || !type.isAssignableFrom(instance.getClass())) {
            if (type.isAssignableFrom(TaskRepository.class)) {
                instance = new TaskRepository();
                if (context != null) {
                    ((TaskRepository) instance).init(((MyApp) context.getApplicationContext()).getFileDataStorage());
                }
            } else {
                try {
                    instance = type.newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return (IRepository<V>) instance;
    }

    public static synchronized <V> IRepository<V> getRepository(Class<? extends IRepository<V>> type) {
        return getRepository(type, null);
    }
}
