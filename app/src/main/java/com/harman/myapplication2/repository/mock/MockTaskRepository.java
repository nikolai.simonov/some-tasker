package com.harman.myapplication2.repository.mock;

import android.support.annotation.Nullable;

import com.harman.myapplication2.das.MockDataStorage;
import com.harman.myapplication2.model.Task;
import com.harman.myapplication2.repository.IRepository;
import com.harman.myapplication2.utils.BaseObservable;

import java.util.List;

public class MockTaskRepository extends BaseObservable implements IRepository<Task> {

    private final MockDataStorage storage;
    private List<Task> list;

    public MockTaskRepository() {
        storage = new MockDataStorage();
        list = storage.getList();
    }

    @Override
    @Nullable
    public Task get(int id) {
        for (Task t: list) {
            if(t.getId() == id){
                return t;
            }
        }
        return null;
    }

    @Override
    public void save(Task item) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId() == item.getId()) {
                list.get(i).setLabel(item.getLabel());
                list.get(i).setDone(item.isDone());
                return;
            }
        }
        list.add(item);
        storage.setList(list);
    }

    @Override
    public void delete(Task item) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId() == item.getId()) {
                list.remove(i);
                return;
            }
        }
        storage.setList(list);
    }

    @Override
    public List<Task> getAll() {
        return list;
    }

}
