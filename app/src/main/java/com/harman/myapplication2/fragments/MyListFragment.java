package com.harman.myapplication2.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.harman.myapplication2.MyAdapter;
import com.harman.myapplication2.R;
import com.harman.myapplication2.model.Task;
import com.harman.myapplication2.repository.IRepository;
import com.harman.myapplication2.repository.RepositoryFactory;
import com.harman.myapplication2.repository.real.TaskRepository;
import com.harman.myapplication2.utils.Observable;

public class MyListFragment extends Fragment {

    private MyAdapter adapter;
    private IRepository<Task> repository;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        repository = RepositoryFactory.getRepository(TaskRepository.class, this.getContext());

        repository.addListener(this::onRepositoryChanged);
    }

    private void onRepositoryChanged(Observable observable) {
        adapter.setItems(repository.getAll());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        ListView listView = view.findViewById(R.id.list_view);

        adapter = new MyAdapter(this::onTaskChanged);
        listView.setAdapter(adapter);

        adapter.setItems(repository.getAll());
        return view;
    }

    private void onTaskChanged(Task task) {
        repository.save(task);
    }
}
