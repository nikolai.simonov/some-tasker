package com.harman.myapplication2.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.harman.myapplication2.R;

public class AddDialogFragment extends DialogFragment {

    private TaskAddCallback callback;

    public interface TaskAddCallback{
        void onTaskAdd(String title);
    }

    private EditText taskName;
    private Button addButton;
    private Button cancelButton;

    public static AddDialogFragment newInstance(TaskAddCallback callback) {
        AddDialogFragment fragment = new AddDialogFragment();
        fragment.callback = callback;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add, container, false);

        taskName = view.findViewById(R.id.task_name);
        addButton = view.findViewById(R.id.add);
        cancelButton = view.findViewById(R.id.cancel);

        addButton.setOnClickListener(this::onAddClick);
        cancelButton.setOnClickListener((v) -> this.dismiss());

        return view;
    }

    private void onAddClick(View view) {
        callback.onTaskAdd(taskName.getText().toString());
        this.dismiss();
    }
}
