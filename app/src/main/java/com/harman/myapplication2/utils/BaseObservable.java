package com.harman.myapplication2.utils;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseObservable implements Observable {

    private final List<ObserverListener> listeners = new ArrayList<>();

    @Override
    public void addListener(ObserverListener listener) {
        listeners.add(listener);
    }

    @Override
    public void remove(ObserverListener listener) {
        listeners.remove(listener);
    }

    @Override
    public void removeAll() {
        listeners.clear();
    }

    @Override
    public void onChanged() {
        for (ObserverListener l: listeners) {
            l.onChanged(this);
        }
    }
}
