package com.harman.myapplication2.utils;

public interface Observable {
    interface ObserverListener {
        void onChanged(Observable some);
    }
    void addListener(ObserverListener listener);
    void remove(ObserverListener listener);
    void removeAll();

    void onChanged();
}
