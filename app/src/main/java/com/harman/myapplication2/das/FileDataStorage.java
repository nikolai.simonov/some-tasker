package com.harman.myapplication2.das;

import android.util.JsonReader;
import android.util.JsonWriter;

import com.harman.myapplication2.model.Task;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileDataStorage {

    private File tasksFile;

    public FileDataStorage(String path) {
        File dir = new File(path);
        if(dir.isDirectory()) {
            tasksFile = new File(dir, "tasks.json");
            if (!tasksFile.exists()) {
                try {
                    tasksFile.createNewFile();
                    writeTasksToFile(new ArrayList<>());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public List<Task> readTasksFromFile(){
        try (JsonReader reader = new JsonReader(new FileReader(tasksFile))) {
            return readTasks(reader);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public void writeTasksToFile(List<Task> tasks){
        try (JsonWriter writer = new JsonWriter(new FileWriter(tasksFile))){
            writer.setIndent("  ");
            writeTasks(writer, tasks);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<Task> readTasks(JsonReader reader) throws IOException {
        List<Task> messages = new ArrayList<>();

        reader.beginArray();
        while (reader.hasNext()) {
            messages.add(readTask(reader));
        }
        reader.endArray();
        return messages;
    }

    private Task readTask(JsonReader reader) throws IOException {
        int id = -1;
        String label = null;
        boolean isDone = false;

        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            switch (name) {
                case "id":
                    id = reader.nextInt();
                    break;
                case "label":
                    label = reader.nextString();
                    break;
                case "isDone":
                    isDone = reader.nextBoolean();
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }
        reader.endObject();
        return new Task(id, label, isDone);
    }


    private void writeTasks(JsonWriter writer, List<Task> tasks) throws IOException {
        writer.beginArray();
        for (Task message : tasks) {
            writeTask(writer, message);
        }
        writer.endArray();
    }

    private void writeTask(JsonWriter writer, Task task) throws IOException {
        writer.beginObject();
        writer.name("id").value(task.getId());
        writer.name("label").value(task.getLabel());
        writer.name("isDone").value(task.isDone());
        writer.endObject();
    }
}
