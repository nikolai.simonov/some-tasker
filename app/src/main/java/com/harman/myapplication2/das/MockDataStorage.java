package com.harman.myapplication2.das;

import com.harman.myapplication2.model.Task;

import java.util.ArrayList;
import java.util.List;

public class MockDataStorage {

    private List<Task> list;

    public MockDataStorage() {
        list = new ArrayList<>();
        list.add(new Task(0,"task1", true));
        list.add(new Task(1,"task2", false));
        list.add(new Task(2,"task3", false));
        list.add(new Task(3,"task4", false));
    }

    public List<Task> getList() {
        return list;
    }

    public void setList(List<Task> list) {
        this.list = list;
    }
}
